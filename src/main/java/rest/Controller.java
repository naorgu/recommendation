package rest;

import facade.Facade;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.List;

@RestController
public class Controller {

    @RequestMapping(value = "/rec", method = RequestMethod.POST)
    public ResponseEntity<RecommendationOutput> recommend(@RequestBody RecommendationRequest req) {
        Facade f = Facade.getInstance();
        RecommendationOutput out = f.getRecommendations(req);
        return new ResponseEntity<RecommendationOutput>(out, HttpStatus.OK);
    }

    @RequestMapping(value = "/recommenders", method = RequestMethod.GET)
    public ResponseEntity<RecommendersListOutput> getRecommenders() {
        Facade f = Facade.getInstance();
        RecommendersListOutput out = f.getRecommendersList();
        return new ResponseEntity<RecommendersListOutput>(out, HttpStatus.OK);
    }

}