package rest;

/**
 * Created by naorguetta on 4/20/15.
 */
public class RecommenderOutput {
    private String name;

    public RecommenderOutput() {

    }

    public RecommenderOutput(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
