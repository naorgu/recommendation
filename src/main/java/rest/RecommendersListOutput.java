package rest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by naorguetta on 12/28/14.
 */
public class RecommendersListOutput {
    private List<RecommenderOutput> recommenders;

    public RecommendersListOutput() {
        recommenders = new ArrayList<RecommenderOutput>();
    }


    public RecommendersListOutput(List<RecommenderOutput> recommendersList) {
        this.recommenders = recommendersList;
    }

    public List<RecommenderOutput> getRecommenders() {
        return recommenders;
    }

}
