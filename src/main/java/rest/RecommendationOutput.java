package rest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by naorguetta on 12/28/14.
 */
public class RecommendationOutput {
    private long userId;
    private List<Long> recommendations;

    public RecommendationOutput() {
        userId = 0;
        recommendations = new ArrayList<Long>();
    }


    public RecommendationOutput(long userId, List<Long> recommendations) {
        this.userId = userId;
        this.recommendations = recommendations;
    }

    public long getUserId() {
        return userId;
    }

    public List<Long> getRecommendations() {
        return recommendations;
    }


}
