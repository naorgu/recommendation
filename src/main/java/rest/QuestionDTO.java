package rest;

/**
 * Created by naorguetta on 6/6/15.
 */
public class QuestionDTO {

    private long id;
    private long subjectId;
    private int level;

    public QuestionDTO(long id, long subjectId, int level) {
        this.id = id;
        this.subjectId = subjectId;
        this.level = level;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
