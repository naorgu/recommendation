package rest;

import java.util.List;
import java.util.Set;

/**
 * Created by naorguetta on 12/28/14.
 */
public class RecommendationRequest {
    private String experimentId;
    private long userId;
    private List<QuestionDTO> questions;
    private String recommender;

    public String getExperimentId() { return experimentId; }

    public void setExperimentId(String experimentId) { this.experimentId = experimentId; }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public String getRecommender() {
        return recommender;
    }

    public void setRecommender(String recommender) {
        this.recommender = recommender;
    }
}
