package core;

import rest.QuestionDTO;

import java.util.*;

/**
 * Created by naorguetta on 6/6/15.
 */
public class QuestionsList {
    private List<QuestionDTO> questions;

    public QuestionsList() { questions = new LinkedList<QuestionDTO>(); }

    public QuestionsList(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public List<Long> getQuestionIds() {
        List<Long> ans = new LinkedList<Long>();
        for (QuestionDTO question : questions) {
            ans.add(question.getId());
        }
        return ans;
    }

    public List<Long> getSubjects() {
        SortedSet<Long> set=new TreeSet<>();
        for (QuestionDTO question : questions) {
            set.add(question.getSubjectId());
        }
        return new ArrayList<>(set);
    }

    public QuestionsList getQuestionsOfSubject(Long subjectId) {
        List<QuestionDTO> questionsList = new LinkedList<QuestionDTO>();
        for (QuestionDTO question : questions) {
            if (question.getSubjectId() == subjectId) {
                questionsList.add(question);
            }
        }
        return new QuestionsList(questionsList);
    }

    public QuestionsList getQuestionsOfLevel(int minLevel) {
        List<QuestionDTO> questionsList = new LinkedList<QuestionDTO>();
        for (QuestionDTO question : questions) {
            if (question.getLevel() >= minLevel) {
                questionsList.add(question);
            }
        }
        questionsList.sort(new Comparator<QuestionDTO>() {
            public int compare(QuestionDTO o1, QuestionDTO o2) {
                return o1.getLevel() - o2.getLevel();
            }
        });

        return new QuestionsList(questionsList);
    }

    public Long pop() {
        if (questions.size() == 0) {
            return null;
        }
        return questions.remove(0).getId();
    }
}
