package core;

import recommenders.HotOrNotRecommender;

import java.util.*;

/**
 * Created by naorguetta on 12/28/14.
 */
public class RecommendationCore {
    private RecommenderManager rm;

    public RecommendationCore() {
        rm = new RecommenderManager();
    }

    public void addRecommender(HotOrNotRecommender rec) {
        rm.addRecommender(rec);
    }

    public List<Long> getRecommendedItems(String experimentId, long userId, QuestionsList questions, String recommenderName) {
        return rm.getRecommender(recommenderName).recommendQuestions(experimentId, userId, questions);

    }

    public List<String> getRecommendersList() {
        return rm.getRecommendersList();
    }
}