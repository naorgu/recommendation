package core;

import recommenders.HotOrNotRecommender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by naorguetta on 4/1/15.
 */
public class RecommenderManager {
    private List<HotOrNotRecommender> recommenders;

    public RecommenderManager() {
        recommenders = new ArrayList<HotOrNotRecommender>();
    }

    public void addRecommender(HotOrNotRecommender r) {
        recommenders.add(r);
    }

    public List<String> getRecommendersList() {
        List<String> ans = new ArrayList<String>();
        for (HotOrNotRecommender r : recommenders) {
            ans.add(getRecommenderName(r));
        }
        return ans;
    }

    public HotOrNotRecommender getRecommender(String recommenderName) {
        for (HotOrNotRecommender r : recommenders) {
            if (getRecommenderName(r).equals(recommenderName)) {
                return r;
            }
        }
        return null;
    }

    public static String getRecommenderName(HotOrNotRecommender r) {
        return r.toString();
    }

}
