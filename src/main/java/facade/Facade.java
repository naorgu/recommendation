package facade;

import recommenders.ProgressRecommender;
import core.QuestionsList;
import mahout.HotOrNotDataModel;
import recommenders.SimpleCDRecommender;
import core.RecommendationCore;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.model.DataModel;
import recommenders.SubjectsRecommender;
import rest.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by naorguetta on 4/1/15.
 */
public class Facade {

    private static Facade ourInstance = new Facade();
    public static Facade getInstance() {
        return ourInstance;
    }

    private RecommendationCore core;

    private Facade() {
        core = new RecommendationCore();
        try {
            core.addRecommender(new SimpleCDRecommender());
            core.addRecommender(new ProgressRecommender());
            core.addRecommender(new SubjectsRecommender());
        } catch (TasteException e) {
            e.printStackTrace();
        }
    }

    public RecommendationOutput getRecommendations(RecommendationRequest req) {
        String experimentId = req.getExperimentId();
        Long userId = req.getUserId();
        QuestionsList questions = new QuestionsList(req.getQuestions());
        String recommender = req.getRecommender();

        List<Long> recList = core.getRecommendedItems(experimentId, userId, questions, recommender);

        return new RecommendationOutput(userId, recList);
    }

    public RecommendersListOutput getRecommendersList() {
        List<String> recommenderNames = core.getRecommendersList();

        List<RecommenderOutput> recommendersList = new ArrayList<RecommenderOutput>();
        for (String name : recommenderNames) {
            recommendersList.add(new RecommenderOutput(name));
        }

        return new RecommendersListOutput(recommendersList);
    }
}
