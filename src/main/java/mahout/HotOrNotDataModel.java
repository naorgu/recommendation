package mahout;

import org.apache.mahout.cf.taste.impl.model.jdbc.PostgreSQLJDBCDataModel;
import org.apache.mahout.cf.taste.model.DataModel;
import org.postgresql.ds.PGSimpleDataSource;

/**
 * Created by naorguetta on 1/7/15.
 */
public class HotOrNotDataModel  {

    public static PGSimpleDataSource getDataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerName("10.211.55.8");
        dataSource.setUser("postgres");
        dataSource.setPassword("passw0rd");
        dataSource.setDatabaseName("hon");
        return dataSource;
    }

    public static String getSubmissionsTable() {
        return "\"Submissions\"";
    }

    public static String getExperimentIdColumn() { return "\"ExperimentId\""; }

    public static String getUserIdColumn() {
        return "\"UserId\"";
    }

    public static String getSubjectColumn() {
        return "\"SubjectId\"";
    }

    public static String getChosenQuestionIdColumn() {
        return "\"ChosenQuestionId\"";
    }

    public static String getGradeColumn() {
        return "\"grade\"";
    }
}
