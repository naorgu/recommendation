package mahout;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.Recommender;

import java.util.Comparator;

/**
 * Created by naorguetta on 12/28/14.
 */
public class UserRecommendationComparator implements Comparator<Long> {
    private long userId;
    private Recommender recommender;

    public UserRecommendationComparator(long userId, Recommender recommender) {
        this.userId = userId;
        this.recommender = recommender;
    }


    @Override
    public int compare(Long itemId1, Long itemId2) {
        try {
            return Math.round(recommender.estimatePreference(userId, itemId1) - recommender.estimatePreference(userId, itemId2));
        } catch (TasteException e) {
            return 0;
        }
    }
}