package recommenders;

import core.QuestionsList;
import rest.QuestionDTO;

import java.util.List;
import java.util.Set;

/**
 * Created by naorguetta on 6/1/15.
 */
public interface HotOrNotRecommender {
    public List<Long> recommendQuestions(String experimentId, long userId, QuestionsList questionsList);
}
