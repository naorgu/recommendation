package recommenders;

import core.QuestionsList;

import java.util.*;


/**
 * Created by naorguetta on 6/1/15.
 */
public class ProgressRecommender implements HotOrNotRecommender {
    private ProgressDataModel dm;

    public ProgressRecommender() {
        dm = new ProgressDataModel();
    }

    @Override
    public List<Long> recommendQuestions(String experimentId, long userId, QuestionsList questionsList) {
        List<QuestionsList> questionsBySubject = questionsBySubjects(experimentId, userId, questionsList, true);
        return getRecommendedQuestions(questionsBySubject);
    }

    private List<QuestionsList> questionsBySubjects(String experimentId, long userId, QuestionsList questionsList, boolean randomOrder) {
        List<QuestionsList> questionsBySubject = new LinkedList<>();
        for (Long subjectId : questionsList.getSubjects()) {
            QuestionsList subjectQuestions = questionsList.getQuestionsOfSubject(subjectId);
            questionsBySubject.add(userLevelQuestions(experimentId, userId, subjectId, subjectQuestions));
        }
        if (randomOrder) {
            Collections.shuffle(questionsBySubject, new Random(System.nanoTime()));
        }
        return questionsBySubject;
    }

    private List<Long> getRecommendedQuestions(List<QuestionsList> questionsBySubject) {
        List<Long> recommendedQuestions = new LinkedList<>();
        while (questionsBySubject.size() > 0) {
            int i = 0;
            while (i < questionsBySubject.size()) {
                Long questionIdToAdd = questionsBySubject.get(i).pop();
                if (questionIdToAdd != null) {
                    recommendedQuestions.add(questionIdToAdd);
                    i++;
                } else {
                    questionsBySubject.remove(i);
                }
            }
        }
        return recommendedQuestions;
    }

    private QuestionsList userLevelQuestions(String experimentId, Long userId, Long subjectId, QuestionsList questions) {
        int userLevel = userLevel(experimentId, userId, subjectId);
        return questions.getQuestionsOfLevel(userLevel);
    }

    private int userLevel(String experimentId, Long userId, Long subjectId) {
        return dm.getLevel(experimentId, userId, subjectId);
    }

    @Override
    public String toString() {
        return "ProgressRecommender";
    }
}
