package recommenders;

import mahout.HotOrNotDataModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by naorguetta on 6/5/15.
 */
public class ProgressDataModel {

    public ProgressDataModel() {
    
    }

    public int getLevel(String experimentId, long userId, long subjectId) {
        String submissionsTable = HotOrNotDataModel.getSubmissionsTable();
        String experimentIdColumn = HotOrNotDataModel.getExperimentIdColumn();
        String userIdColumn = HotOrNotDataModel.getUserIdColumn();
        String subjectColumn = HotOrNotDataModel.getSubjectColumn();
        String gradeColumn = HotOrNotDataModel.getGradeColumn();
        String stmt = "select count(*) from " + submissionsTable + " where " + experimentIdColumn + "= ? " +
                "and " + userIdColumn + "= ? " + "and " + subjectColumn + "= ? " + "and " + gradeColumn + "=" + "100";
        try (Connection conn = HotOrNotDataModel.getDataSource().getConnection()) {
            PreparedStatement pstmt = conn.prepareStatement(stmt);
            pstmt.setString(1, experimentId);
            pstmt.setLong(2, userId);
            pstmt.setLong(3, subjectId);
            ResultSet results = pstmt.executeQuery();
            results.next();
            return results.getInt(1);
        } catch (SQLException e) {
            System.out.println(e);
        }

        return 0;
    }
}
