package recommenders;

import core.QuestionsList;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by naorguetta on 6/6/15.
 */
public class SubjectsRecommender implements HotOrNotRecommender {
    @Override
    public List<Long> recommendQuestions(String experimentId, long userId, QuestionsList questionsList) {
        List<Long> recommendedQuestions = new LinkedList<>();

        List<QuestionsList> questionsBySubject = new LinkedList<>();
        for (Long subjectId : questionsList.getSubjects()) {
            QuestionsList subjectQuestions = questionsList.getQuestionsOfSubject(subjectId);
            questionsBySubject.add(subjectQuestions.getQuestionsOfLevel(0));
        }
        for (QuestionsList subjectQuestions : questionsBySubject) {
            recommendedQuestions.addAll(subjectQuestions.getQuestionIds());
        }

        return recommendedQuestions;
    }

    @Override
    public String toString() {
        return "SubjectsRecommender";
    }
}
