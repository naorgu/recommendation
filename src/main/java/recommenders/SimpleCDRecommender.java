package recommenders;

import java.util.*;

import mahout.HotOrNotDataModel;
import mahout.UserRecommendationComparator;
import org.apache.mahout.cf.taste.impl.model.jdbc.PostgreSQLJDBCDataModel;
import recommenders.HotOrNotRecommender;
import core.QuestionsList;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

/**
 * A simple {@link Recommender} implemented for the Jester Online Joke
 * Recommender data set demo. See the <a
 * href="http://eigentaste.berkeley.edu/dataset/">Jester site</a>.
 */
public class SimpleCDRecommender implements HotOrNotRecommender {

    private final Recommender recommender;
    private UserNeighborhood neighborhood;
    private UserSimilarity similarity;

    private DataModel model;


    public SimpleCDRecommender() throws TasteException {
        model = new PostgreSQLJDBCDataModel(HotOrNotDataModel.getDataSource(), HotOrNotDataModel.getSubmissionsTable(),
                HotOrNotDataModel.getUserIdColumn(), HotOrNotDataModel.getChosenQuestionIdColumn(), HotOrNotDataModel.getChosenQuestionIdColumn(), null);
        similarity = new PearsonCorrelationSimilarity(model);
        neighborhood = new NearestNUserNeighborhood(20, similarity, model);
        recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
    }


    @Override
    public String toString() {
        return "SimpleCDRecommender";
    }

    @Override
    public List<Long> recommendQuestions(String experimentId, long userId, QuestionsList questionsList) {
        List<Long> orderedIds = new ArrayList<Long>();
        orderedIds.addAll(questionsList.getQuestionIds());

        Comparator<Long> comp = new UserRecommendationComparator(userId, recommender);
        orderedIds.sort(comp.reversed());

        return orderedIds;
    }
}