package core;

import junit.framework.TestCase;
import rest.QuestionDTO;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by naorgu on 9/5/15.
 */
public class QuestionsListTest extends TestCase {

    private QuestionsList questionsList;

    public void setUp() throws Exception {
        super.setUp();
        QuestionDTO q1 = new QuestionDTO(1, 1, 1);
        QuestionDTO q2 = new QuestionDTO(2, 1, 1);
        QuestionDTO q3 = new QuestionDTO(3, 1, 1);
        List<QuestionDTO> questionDTOs = new LinkedList<>(Arrays.asList(q1, q2, q3));

    }

    public void testGetQuestionIds() throws Exception {
        QuestionDTO q1 = new QuestionDTO(1, 1, 1);
        QuestionDTO q2 = new QuestionDTO(2, 1, 1);
        QuestionDTO q3 = new QuestionDTO(3, 1, 1);
        List<QuestionDTO> questionDTOs = new LinkedList<>(Arrays.asList(q1, q2, q3));
        QuestionsList questionsList = new QuestionsList(questionDTOs);

        List<Long> questionIds = questionsList.getQuestionIds();
        assertEquals(questionIds.get(0).intValue(), 1);
        assertEquals(questionIds.get(1).intValue(), 2);
        assertEquals(questionIds.get(2).intValue(), 3);
    }

    public void testGetSubjects() throws Exception {
        QuestionDTO q1 = new QuestionDTO(1, 1, 1);
        QuestionDTO q2 = new QuestionDTO(2, 1, 1);
        QuestionDTO q3 = new QuestionDTO(3, 1, 1);
        List<QuestionDTO> questionDTOs = new LinkedList<>(Arrays.asList(q1, q2, q3));
        QuestionsList questionsList = new QuestionsList(questionDTOs);

        List<Long> questionSubjects = questionsList.getSubjects();
        assertEquals(questionSubjects.get(0).intValue(), 1);
        assertEquals(questionSubjects.get(1).intValue(), 1);
        assertEquals(questionSubjects.get(2).intValue(), 1);
    }

    public void testGetQuestionsOfSubject() throws Exception {

    }

    public void testGetQuestionsOfLevel() throws Exception {

    }

    public void testPop() throws Exception {

    }
}