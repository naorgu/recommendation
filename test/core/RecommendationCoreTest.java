package core;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecommendationCoreTest {

    private RecommendationCore core;
    private Recommender rec;

    @Before
    public void setUp() {
        core = new RecommendationCore();

        try {
            rec = RecommenderManagerTest.createGenericUserBasedRecommender();
            core.addRecommender(rec);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TasteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetRecommendedItems() throws Exception {
        long userId = 1;
        Set<Long> itemIds = new HashSet<Long>(Arrays.asList(new Long(1), new Long(2), new Long(3)));
        String recommender = rec.toString();

        List<Long> recommendations = core.getRecommendedItems(userId, itemIds, recommender);

        assertEquals(recommendations.size(), itemIds.size());
    }

    @Test
    public void testGetRecommendersList() throws Exception {
        List<String> recList = core.getRecommendersList();
        System.out.print(recList);
        assertEquals(recList.size(), 1);
    }
}