package core;

import mahout.UserRecommendationComparator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class UserRecommendationComparatorTest {

    private UserRecommendationComparator comp;

    @Before
    public void setUp() throws Exception {
        DataModel model = new FileDataModel(new File("dataset.csv"));
        UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, model);
        Recommender rec = new GenericUserBasedRecommender(model, neighborhood, similarity);

        comp = new UserRecommendationComparator(1, rec);
    }

    @Test
    public void testCompare() throws Exception {
        int x = comp.compare(new Long(10), new Long(11));
        assertEquals(x, -1);
    }
}