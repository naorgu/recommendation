package core;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class RecommenderManagerTest {

    private RecommenderManager rm;

    @Before
    public void setUp() throws Exception {
        rm = new RecommenderManager();
    }

    @Test
    public void testAddGetRecommender() throws Exception {
        Recommender rec = createGenericUserBasedRecommender();
        rm.addRecommender(rec);
        assertEquals(rm.getRecommender(rec.toString()), rec);
    }

    @Test
    public void testGetRecommendersList() throws Exception {
        Recommender rec = createGenericUserBasedRecommender();
        rm.addRecommender(rec);
        List<String> recList = rm.getRecommendersList();
        assertEquals(recList.size(), 1);
        assertEquals(recList.get(0), rec.toString());
    }

    public static Recommender createGenericUserBasedRecommender() throws IOException, TasteException {
        DataModel model = new FileDataModel(new File("dataset.csv"));
        UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, model);

        return new GenericUserBasedRecommender(model, neighborhood, similarity);
    }
}